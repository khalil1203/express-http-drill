const express = require('express');
const app = express();
const { PORT } = require('./config');
const http = require('http');
const fs = require('fs');
const path = require('path');
const uuid = require("uuid");
const requestId = require('express-request-id');
const router = express.Router();


app.use(requestId());

app.use((req, res, next) => {

    let url = req.url;
    if (url != "/favicon.ico") {
        const content = url + "          ,    Request Id :  " + req.id + "         , Time : " + new Date() + '\n';
        fs.appendFile('log.log', content, (err) => {
            if (err) {
                console.log(err);
            }
        });
    }
    next();

});

app.use(router);


router.get('/html', (req, res) => {
    res.sendFile(path.join(__dirname, "data", "page.html"));
});




router.get('/log', (req, res) => {
    res.sendFile(path.join(__dirname, "log.log"));
});


router.get('/json', (req, res) => {
    let arr = req.url.split("/");
    res.sendFile(path.join(__dirname, "data", "page2.json"));
});


router.get('/uuid', (req, res) => {
    let generate_uuid = uuid.v4()
    res.json({ uuid: generate_uuid });
});

router.get('/status/:id', (req, res, next) => {
    const status = req.params.id;
    if (!http.STATUS_CODES[status] || status == 100) {
        // return res.send("Do not provide wrong status code!!!!")
        next({
            message: "Do not provide wrong status code!!!!"
        })
    }
    else {
        res.status(status);
        res.json({
            statuscode: status,
            message: http.STATUS_CODES[status]
        });

    }
});



router.get('/delay/:id', (req, res, next) => {
    let delay_Time = req.params.id;
    if (delay_Time >= 0) {
        setTimeout(() => {
            res.json({
                Time: delay_Time,
                message: `Sorry for wasting your ${delay_Time} seconds!!!!`
            });
        }, delay_Time * 1000);

    }
    else {

        next({
            message: "Do not provide wrong timeline!!!!"
        })
    }
});


router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, "data", "homepage.html"));
});

router.use((err, req, res, next) => {
    res.status(400).json(err);
})


app.use((req, res) => {
    res.status(400).sendFile(path.join(__dirname, "data", "error.html"));
})



app.listen(PORT, (err) => {
    if (err) {
        console.log("oops server is not showing up!!!!");
    }
    else {
        console.log("server is up and running on port : ", PORT);
    }
});
